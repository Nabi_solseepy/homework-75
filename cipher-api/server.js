const express = require('express');
const app = express();
const cipher = require('./app/cipher');
const cors = require('cors');
const port = 8000;
app.use(express.json());

app.use(cors());
app.use('/', cipher);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});
import {DECODE_HUNDLER, ENCODE_HENDLER, PASSWORD_CANGE, POST_DECODE_SUCCESS, POST_ENCODE_SUCCESS} from "./action";

const initialState = {
   encode: '',
   decode: '',
   password: ''
};

const reducer = (state = initialState, action ) => {
    switch (action.type) {
        case POST_ENCODE_SUCCESS:
            return {
                ...state,
                decode: action.encode
            };
        case POST_DECODE_SUCCESS:
            return {
                ...state,
                encode: action.decode
            };
        case DECODE_HUNDLER:
            return {...state, encode: action.decode};
        case ENCODE_HENDLER:
            return {
               ...state,
                encode: action.encode
            };
        case PASSWORD_CANGE:
            return {
                ...state,
                password: action.password
            };
        default:
            return state
    }
};

export default reducer
import axios from '../axios.api'
export const POST_ENCODE_SUCCESS = 'POST_ENCODE_SUCCESS';
export const POST_DECODE_SUCCESS = 'POST_DECODE_SUCCESS';


export const DECODE_HUNDLER = 'DECODE_HUNDLER';
export const ENCODE_HENDLER = 'ENCODE_HENDLER';
export const PASSWORD_CANGE = 'PASSWORD_CANGE';




export  const encodeToPost = encode => ({type: POST_ENCODE_SUCCESS, encode });
 export const decodeToPost = decode => ({type: POST_DECODE_SUCCESS, decode});

export const passwordChange = event => {
    return {type: PASSWORD_CANGE, password: event.target.value}
};

export const decodeHandler = event => {
    return {type: DECODE_HUNDLER, decode: event.target.value}
};


export const encodeHandler = event => {
    return {type: ENCODE_HENDLER, encode: event.target.value}
};



export const encodePost = (encode) => {
    return dispatch => {
        axios.post('encode', encode).then(response => {
            dispatch(encodeToPost(response.data.encode))
        })

    }
};

export const postDecode = decode => {
    return dispatch => {
        axios.post('decode', decode).then(response => {
            console.log(response);
            dispatch(decodeToPost(response.data.decode))
        })
    }
};
import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {decodeHandler, encodeHandler, encodePost, passwordChange, postDecode} from "../../store/action";

class Forms extends Component {
    inputChenge = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };


    decodeHandler = () => {
        this.props.postToDecode({password: this.props.password, message: this.props.decode} )
    };
    encodeHandler = () => {
        this.props.postToEncode({password: this.props.password, message: this.props.encode} )
    };


    render() {
        return (
                <Form>
                    <FormGroup row>
                        <Label for="exampleText" sm={2}>Decoded message</Label>
                        <Col sm={10}>
                            <Input type="textarea" name="decode" value={this.props.encode} onChange={this.props.encodeChange} id="exampleText" />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="examplePassword" sm={2}>Password</Label>
                        <Col sm={8}>
                            <Input type="text" name="password" id="examplePassword" value={this.props.password} onChange={this.props.passwordChange} placeholder="password placeholder" />
                        </Col>
                        <Col>
                            <Button onClick={this.encodeHandler}>decode</Button>
                            <Button onClick={this.decodeHandler}>encode</Button>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label for="exampleText" sm={2}> Eencoded message</Label>
                        <Col sm={10}>
                            <Input type="textarea" onChange={this.props.decodeHandler} value={this.props.decode} name="dencode" id="exampleText" />
                        </Col>
                    </FormGroup>
                </Form>

        );
    }
}

const mapStateToProps = state => ({
 encode: state.encode,
 password: state.password,
 decode: state.decode

});

const mapDispatchToProps = dispatch => ({
    postToEncode: (encode) => dispatch(encodePost(encode)),
    postToDecode: (decode) => dispatch(postDecode(decode)),
    decodeHandler: event => dispatch(decodeHandler(event)),
    encodeChange: event => dispatch(encodeHandler(event)),
    passwordChange: event => dispatch(passwordChange(event))

});


export default connect(mapStateToProps, mapDispatchToProps)(Forms);